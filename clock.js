$(function() {

    let canvas = document.getElementById('analog_clock');
    let ctx = canvas.getContext('2d');


    let $switch = 0;
    let now;
    let hour;
    let minute;
    let second;
    let $format = "12";
    let clock;

    let alarm_set;
    let stop_alarm_count = 0;

    let radius = canvas.height / 2;
    ctx.translate(radius, radius);
    radius = radius * 0.90;

    analog_clock();

    //Switching between Analog & Digital
    $("#switch").click(function() {
        $("#format").toggle();
        if (!$switch) {
            $("#headings").text("DIGITAL CLOCK");
            $("#switch").text("Analog Clock");
            $switch = 1;
            digital_clock();
        } else {
            $("#headings").text("ANALOG CLOCK");
            $("#switch").text("Digital Clock");
            $switch = 0;
            analog_clock();
        }

    });

    // Format changing
    $("#format").click(function() {
        $format = $(this).val();
    });

    function getTime() {
        now = new Date();
        hour = now.getHours();
        minute = now.getMinutes() < 10 ? ('0' + now.getMinutes()) : (now.getMinutes());
        second = now.getSeconds() < 10 ? ('0' + now.getSeconds()) : (now.getSeconds());
    }

    function digital_clock() {
        clearInterval(clock);
        $(canvas).hide();
        getTime();
        $("#digital_clock").show();
        let $period = ($format == "24") ? (" ") : ((hour > 11) ? ("PM") : ("AM"));
        let hour1 = ($format == "24") ? (hour) : ((hour > 12) ? (Number(hour % 12)) : (Number(hour)));
        $("#digital_clock").text(hour1 + ":" + minute + ":" + second + " " + $period);
        clock = setInterval(digital_clock, 1000);
    }

    function analog_clock() {
        clearInterval(clock);
        $("#digital_clock").hide();

        clock = setInterval(drawClock, 1000);
        $(canvas).show();

        function drawClock() {
            drawFace(ctx, radius);
            drawNumbers(ctx, radius);
            drawTime(ctx, radius);
        }
        ctx.shadowOffsetX = 4;
        ctx.shadowOffsetY = 4;
        ctx.shadowBlur = 7;
        ctx.shadowColor = "gray";

        function drawFace(ctx, radius) {
            ctx.beginPath();
            ctx.arc(0, 0, radius, 0, 2 * Math.PI);
            ctx.fillStyle = "#ffe9d4";
            ctx.fill();

            ctx.lineWidth = radius * 0.1;
            ctx.stroke();

            ctx.beginPath();
            ctx.arc(0, 0, radius * 0.1, 0, 2 * Math.PI);
            ctx.fillStyle = "#333";
            ctx.fill();
        }

        function drawNumbers(ctx, radius) {
            let ang;
            let num;
            ctx.font = radius * 0.15 + "px arial";
            ctx.textBaseline = "middle";
            ctx.textAlign = "center";
            for (let num = 1; num < 13; num++) {
                ang = num * Math.PI / 6;
                ctx.rotate(ang);
                ctx.translate(0, -radius * 0.85);
                ctx.rotate(-ang);
                ctx.fillText(num.toString(), 0, 0);
                ctx.rotate(ang);
                ctx.translate(0, radius * 0.85);
                ctx.rotate(-ang);
            }
        }

        function drawTime(ctx, radius) {
            getTime();

            //hour
            let hour1 = hour % 12;
            hour1 = (hour1 * Math.PI / 6) + (minute * Math.PI / (6 * 60)) + (second * Math.PI / (360 * 60));
            drawHand(ctx, hour1, radius * 0.5, radius * 0.03, "black");

            //minute
            minute1 = (minute * Math.PI / 30) + (second * Math.PI / (30 * 60));
            drawHand(ctx, minute1, radius * 0.8, radius * 0.03, "black");

            //second
            second1 = (second * Math.PI / 30);
            drawHand(ctx, second1, radius * 0.9, radius * 0.01, "red");
        }

        function drawHand(ctx, pos, length, width, color) {
            ctx.shadowColor = "transparent";
            ctx.beginPath();
            ctx.lineWidth = width;
            ctx.lineCap = 'round';
            ctx.moveTo(0, 0);
            ctx.rotate(pos);
            ctx.lineTo(0, -length);
            ctx.strokeStyle = color;
            ctx.stroke();
            ctx.strokeStyle = "black";
            ctx.rotate(-pos);
        }
    }

    $('#set_alarm').click(function() {
        let time = $("#alarm").val();
        // Storing in local storage
        if (typeof(Storage) !== "undefined") {
            localStorage.setItem("alarm", time);
            alarm_set = true;
            isAlarmSet();
        } else {
            $("#alarm_time").text("Sorry, your browser does not support Web Storage...");
        }
        $('#myModal').modal('toggle');
    });

    function isAlarmSet() {
        if (localStorage.getItem("alarm") != undefined) {
            alarm_set = true;
            $("#alarm_time").text("Alarm set for: " + localStorage.getItem("alarm"));
            $("#alarm_info").append('<button type="button" class="btn btn-outline-danger" id="delete_button">Delete Alarm</button>');
            $("#create_alarm").prop('disabled', true);

            let delete_button = document.querySelector('#delete_button');

            delete_button.addEventListener('click', function() {
                $("#create_alarm").prop('disabled', false);
                $("#alarm_sound")[0].pause();
                $("#alarm_info").hide();
                localStorage.removeItem("alarm");
                alarm_set = false;
                location.reload();
            });
        } else {
            alarm_set = false;
        }
    }


    function checkAlarm() {
        if (alarm_set) {
            timeOfAlarm = localStorage.getItem("alarm");
            let arr = timeOfAlarm.split(":");
            let hr = arr[0];
            let min = arr[1];

            getTime();

            if (hr == hour && min == minute) {
                $("#alarm_sound")[0].play();
                console.log(stop_alarm_count)
                if (stop_alarm_count == 0) {
                    stop_alarm_count += 1;
                    $("#alarm_info").append('<button type="button" class="btn btn-outline-danger" id="stop_button">Stop Alarm</button>');

                    let stop_button = document.querySelector('#stop_button');

                    stop_button.addEventListener('click', function() {
                        $("#alarm_sound")[0].pause();
                        $("#stop_button").hide();
                        alarm_set = false;
                    });
                }
            }
        }
    }

    setInterval(checkAlarm, 1000);
});